.. _descargas-index:

Documentos en PDF para descargar
================================

Aquí encontrará documentos en formato PDF que podrá descargar para sus actividades.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    descargas/descargas

Indices and Tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
