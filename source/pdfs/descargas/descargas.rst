Guia del Profesor PDF
=====================

:download:`Guia de ejemplo <./GuiaProfesor.pdf>`

Manual Técnico
==============

:download:`Manual Técnico <./Manual Tecnico.pdf>`

Guión Niños Zanahoria
=====================

:download:`Guión Niños Zanahoria <./GuionesNinosZanahoria.pdf>`
