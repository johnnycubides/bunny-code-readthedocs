.. _ejemplos-index:

Ejemplos de uso
====================

Aquí incontrará información que le permita hacer uso de BunnyCode en sus cursos.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    matematicas/matematicas

Indices and Tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
