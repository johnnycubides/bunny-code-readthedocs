.. _guia_profesires-index:

Guia para profesores
====================

Aquí incontrará información que le permita hacer uso de BunnyCode en sus cursos.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    guia/guia

Indices and Tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
