Guia Del Profesor
=================

.. image:: imag/bunnycode_genral.png

Resumen
=======

En esta cartilla se presenta una herramienta pedagógica para desarrollar ciertas capacidades en los niños
entre 4 a 6 años, con base a las competencias educativas exigidas en el modelo colombiano, complementado
con los puntos fuertes de los modelos educativos de los países estrella el área de educación. Se introduce
entonces a BunnyCode, un sistema electrónico formado por piezas tipo rompecabezas, una base central y un
robot conejo que sigue una serie de tareas escogidas por los niños. Para esta guía, se sugiere una serie
de actividades para realizar con el sistema, con el objetivo de desarrollar dichas capacidades fundamentales
en los niños.

Introducción
============

Gracias al desarrollo tecnológico actual que se está evidenciando, complementar el ejercicio pedagógico con herramientas tecnológicas ha sido una prioridad en gran parte de los modelos educativos mundiales. Las herramientas tecnológicas, a diferencia de las tradicionales, no le exigen al profesor una permanencia constante en el aula de clase, asimismo como su participación activa durante el desarrollo de las actividades. En adición, le permiten al niño una conexión directa con el mundo a través del internet, donde existe una vasta cantidad de información. Para los expertos en pedagogía, la prioridad del uso de dichas herramientas es con el fin de desarrollar las llamadas habilidades del siglo XXI, que son: Pensamiento crítico/Resolución de problemas, Creatividad, comunicación y Trabajo corporativo.

Estas habilidades responden al acelerado crecimiento económico, la abundancia de información y a la necesidad de sobresalir en un entorno laborar altamente competitivo. Para suplir dicha necesidad, en el mercado han surgido múltiples herramientas que permiten desarrollar dichas habilidades desde los 3 años, aprovechando la facilidad de los niños de retener información en edades tempranas. Entre ellas podemos encontrar aplicaciones que permiten dar a los estudiantes un acercamiento a conceptos básicos de programación y al desarrollo de un pensamiento estructurado. Favorablemente, este tipo de herramientas ha llamado la atención tanto de padres como de entidades educativas, pero en algunos casos no es recomendable por la alta exposición a pantallas.

Por ende, se propone el uso de medios tangibles para la enseñanza de los niños. Para este caso, se busca enseñar a los niños principios de programación por medio de bloques, favoreciendo la interacción corporal y reduciendo el tiempo frente a la pantalla. Este proceso se asemeja a actividades como el ensamble de un rompecabezas. Estimulando así el desarrollo cognitivo y motriz del niño. Existen diseños pedagógicos de grandes empresas como Fisher Price y Google con el mismo objetivo, pero igual siguen presentando pequeñas desventajas.

En consecuencia, se presenta a BunnyCode, una herramienta pedagógica que permite relacionar a los niños con los conceptos básicos de programación de manera divertida y llamativa con el fin de desarrollar dichas habilidades aplicadas al hogar y en el aula de clase. La versatilidad del sistema permite fomentar el aprendizaje en áreas como las matemáticas y el lenguaje, fundamentales para el desarrollo del niño. Cabe resaltar que algunos planes pedagógicos hacen un gran enfoque a alternativas no convencionales para fomentar el aprendizaje en dichas áreas con el uso del aprendizaje basado en proyectos (ABP), el cual va en contraparte de los métodos pedagógicos tradicionales. BunnyCode también se diseña de tal manera que sea accesible a todos, de modo que fomente la inclusión en la pedagogía y en la educación.
Las habilidades a desarrollar con BunnyCode serán la lateralidad, el trabajo en equipo, la percepción visual, coordinación ojo-mano, entre otros. Todo esto espera darse a través del juego.

El dispositivo electrónico se encuentra conformado por un conjunto de bloques, una tarjeta principal y un robot, que conectadas en conjunto siguen una serie de instrucciones predeterminadas por el niño. La versatilidad del sistema permite también crear historias, entre un sinfín de combinaciones que darán lugar a la imaginación del niño.

Finalmente, a usted profesor, queremos darle las gracias por hacer parte de este hermoso proyecto que busca facilitar el aprendizaje estructurado de los niños. El papel del educador en este proceso pedagógico es fundamental para a orientación del niño, asimismo para el encaminamiento de las ideas y la incentivación de la creación de un mundo imaginativo.
Grupo de desarrollo BunnyCode


Objetivos
=========

General
+++++++

El objetivo principal con esta cartilla es dar a conocer al profesor qué es BunnyCode, lo que se quiere lograr con él y dar las bases esenciales para poder construir el modelo de enseñanza a los niños basado en las competencias generales que se deben adquirir en la edad de 4 a 6 años, entre ellas las ideas básicas de programación.

Específicos
+++++++++++

1. Brindar al profesor las actividades a realizar con BunnyCode
2. Explicar el funcionamiento del sistema
3. Hacer una breve sustentación del modelo pedagógico que se quiere implementar con dicho sistema.

Justificación
=============

Como se había mencionado antes en la introducción, la creación de BunnyCode responde a una fuerte necesidad de educar a los niños desde edad temprana en áreas informáticas con el fin de desarrollar las habilidades del siglo XXI. Con estas, se busca que el niño logre desarrollar un pensamiento estructurado y lógico ante los problemas que se le presentarán en el transcurso de la vida. Como el sistema posibilita el desarrollo de la creatividad en los niños, permitirá entonces ejercitar la imaginación, dentro de un entorno donde la electrónica facilita la construcción de ideas y rutas personalizables por el niño. La abstracción del sistema en conjunto se basa en un inicio y un final, lo que le da la libertad al niño de crear su camino con base a sus necesidades y recursos. Esto, visto como una meta podría extrapolarse a la adultez donde le permitiría ser más organizado y específico con las cosas que desea lograr.

¿Qué es BunnyCode?
==================

BunnyCode nació de la idea de dos jóvenes: Ana Moreno y Nelson Tovar, estudiantes de ingeniería electrónica de la Universidad Nacional de Colombia, que buscaban la manera de enseñarles a programar a los niños saliendo de los métodos convencionales de aprendizaje. Acá todo se centra en el concepto de tangibilidad, es decir que los niños puedan tocar, interactuar y armar su propio algoritmo. La idea con BunnyCode es que a partir de unas fichas armar la ruta que debe seguir el conejo, pero esta debe tener un inicio y un final.

En síntesis, BunnyCode es una herramienta que permite desarrollar el pensamiento lógico en niños desde temprana edad, ésto a través de la programación tangible, con la cual se pueden implementar simples programas de computador sin que el niño requiera aprender toda la sintaxis de un lenguaje de programación.

Partes del Sistema
==================

Robot
+++++

El robot es el componente del sistema que se encarga de ejecutar el algoritmo implementado con el set de instrucciones.

.. image:: imag/bunny.png

Piezas Tangibles como set de Instrucciones
++++++++++++++++++++++++++++++++++++++++++

La programación tangible se desarrolla con Bunny Code gracias a la conexión secuencial de distintas piezas que pueden ser conectadas entre sí. Hay dos set de fichas.
El primer set de fichas representan instrucciones, inicio y fin del algoritmo implementado.

.. image:: imag/piezas.png

La función de cada ficha se describe a continuación:

.. image:: imag/set_instrucciones.png

El segundo set de fichas representa distintos apartes de alguna historia o cuento infantil, que conectándose apropiadamente permiten escuchar la narración de dicha historia.

.. image:: imag/historias_general.png

Modulo central
++++++++++++++

El set de instrucciones se conecta al módulo central, el cual se encarga de procesar las instrucciones y enviarlas al robot.

.. image:: imag/esquema_genral.png

Plan de Actividades
===================

Este plan de actividades se realiza con base a los derechos básicos de aprendizaje (DBA) de Colombia, complementado con los modelos educativos de alguno de los países más influyentes en el área de educación. Estos constituyen el conjunto de aprendizajes estructurantes que desarrollan los niños y niñas por medio de experiencias y ambientes pedagógicos como el juego, el arte, la exploración del exterior y la literatura. Estos DBAs permiten a la comunidad académica identificar qué conocimientos deben adquirir los estudiantes en cada grado. A continuación se exponen las características sugeridas para que los profesores evalúen conforme se utiliza BunnyCode.

Conclusiones
============

Pra concluir, basta con agradecer su ayuda en este proceso de desarrollo de habilidades en los niños que favorecen el crecimiento mental, el pensamiento estructurado y la resolución de problemas lógicos. Cabe resaltar que todo esto se basa en un contexto donde la programación a temprana edad está jugando un papel importante, debido a que complementa los llamados saberes del siglo XXI. Por otro lado, para que los objetivos que tiene el profesor hacia los niños se logren de manera satisfactoria, se sugiere utilizar los ejemplos de uso de BunnyCode, así como los Storytellings, lo cual facilitará en primera medida conocer la programación tangible, segundo aprender a llevar un orden para lograr las metas y finalmente para identificar problemas que hacen que los objetivos iniciales no se desarrollen a cabalidad. Finalmente, resaltar la importancia de los niños en la construcción de sociedad en el futuro; una generación educada será menos vulnerable a ser engañada y tendrá argumentos para defenderse en situaciones donde lo amerite. 
ara concluir, basta con agradecer su ayuda en este proceso de desarrollo de habilidades en los niños que favorecen el crecimiento mental, el pensamiento estructurado y la resolución de problemas lógicos. Cabe resaltar que todo esto se basa en un contexto donde la programación a temprana edad está jugando un papel importante, debido a que complementa los llamados saberes del siglo XXI. Por otro lado, para que los objetivos que tiene el profesor hacia los niños se logren de manera satisfactoria, se sugiere utilizar los ejemplos de uso de BunnyCode, así como los Storytellings, lo cual facilitará en primera medida conocer la programación tangible, segundo aprender a llevar un orden para lograr las metas y finalmente para identificar problemas que hacen que los objetivos iniciales no se desarrollen a cabalidad. Finalmente, resaltar la importancia de los niños en la construcción de sociedad en el futuro; una generación educada será menos vulnerable a ser engañada y tendrá argumentos para defenderse en situaciones donde lo amerite.



