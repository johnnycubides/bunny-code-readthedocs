.. BunnyCode documentation master file, created by
   sphinx-quickstart on Tue Jun  5 03:33:01 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BunnyCode's documentation!
=====================================

.. image:: imag/bunnycode_genral.png

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    guia_profesores/index_guia_profesores
    ejemplos/index_ejemplos
    manual/index_manual
    pdfs/index_descargas



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
