¿Qué es BunnyCode?
==================

BunnyCode es una herramienta que permite desarrollar el pensamiento lógico en niños desde
temprana edad, esto a través de la programación tangible, con la cual se pueden implementar
programas simples de computador sin que el niño requiera aprender toda la sintaxis de un lenguaje
de programación.

Partes del sistema
++++++++++++++++++

Las partes que conforman la totalidad del sistema se detallan a continuación

Robot
+++++

El robot es el componente del sistema que se encarga de ejecutar el programa implementado
con el set de instrucciones sobre el módulo central.
El robot posee conectividad con el módulo central, motores para el movimiento, sensores de
sonido, infrarrojo y luz, un sistema con Linux embebido y un microcontrolador STM32F100. El
diagrama de bloques del sistema electrónico es mostrado a continuación

.. image:: imag/diagram_bloques.png

El sistema electrónico y mecánico se encuentra cubierto por una carcasa con forma de conejo, ver
la siguiente figura.

.. image:: imag/bunny.png

Piezas Módulo Centraezas Módulo Central
+++++++++++++++++++++++++++++++++++++++

La programación tangible se desarrolla con BunnyCode gracias a la conexión secuencial de
distintas piezas que pueden ser encajadas entre sí. Hay dos sets de fichas.
El primer set de fichas representa estructuras de control de programa, desplazamientos, y
lectura de sensores. En la figura 3 se hace una descripción del set de instrucciones.

.. image:: imag/set_instrucciones.png

El segundo set de fichas representa distintos apartes de alguna historia o cuento infantil, que
conectándose apropiadamente permiten escuchar la narración de dicha historia. En la figura 4 se
muestra, el set de instrucciones para la historieta de *Los Tres Cerditos*.

Descripción del sistema de conexión
-----------------------------------

Cada pieza del set de fichas se conecta mediante conectores DB9 de 9 clavijas. Dichos conectores
hace que la conexión de las piezas sea muy fácil. En la siguiente figura se muestra el conector y la
descripción de pines.

.. image:: imag/conexion0.png

.. image:: imag/conexion1.png

Módulo Central
++++++++++++++

El set de instrucciones se conecta al módulo central, el cual se encarga de procesar las instrucciones
y enviarlas al robot. En la figura 6 se muestra el módulo central con un ejemplo de
conexión de los sets de instrucciones.

Disposición
-----------

En la siguiente imagen se observa un diagrama que indica los componentes del módulo y su
conexión. A continuación se muestra el diagrama de bloques del módulo central.

.. image:: imag/diagram_bloques2.png

Recomendaciones de Uso
======================

A continuación se realizan una serie de recomendaciones para garantizar el correcto funcionamiento
y la operación segura de BunnyCode.

* Asegurarse de leer todas la instrucciones antes del uso del juguete.

* Asegurarse de revisar de manera periódica todas las piezas del juguete, si encuentra algún
  daño o componente suelto, acercarse al centro de atención para el mantenimiento del
  juguete.

* Recuerde que las actividades realizadas con los niños siempre deben tener la supervisión
  de un adulto responsable.

* Las piezas del set de fichas tiene un tamaño con dimensiones pequeñas, por lo tanto, el
  juguete no es apto para menores de 3 años.

* Después de usar el juguete guarde todas sus piezas y componentes en su estuche, la pérdida
  de alguna pieza pueda inhabilitar la funcionalidad adecuada del juguete.

Operaciones Comunes
===================

El control dentro del sistema es ejercido por el módulo principal, ya que este constituye el
medio para acceder a las funciones más básicas para controlar el conejo: hacer que se mueva y
que pare. Entre estas tenemos el encendido, la programación y el apagado.

Encendido
+++++++++

Conecte el módulo principal a una fuente de voltaje DC de 5V a 1A para mejor rendimiento.
No exceder este voltaje ya que podría causar daños irreversibles en el sistema. Luego encienda el
módulo por medio del switch que se encuentra en la parte lateral derecha. Finalmente encienda el
conejo por medio del switch. Asegúrese que este tenga baterías (AA) y se encuentren cargadas. Es
indispensable que ambos aparatos se encuentren encendidos para que la comunicación se realice
de manera correcta.

Programación
++++++++++++

Para la ejecución del programa en el conejo se necesita presionar un botón ubicado en la cara
frontal del módulo principal. Luego de construir la secuencia lógica con las piezas se presiona
el botón. Si el LED aparece como rojo es porque el programa no fue ejecutado correctamente;
verifique que la secuencia tenga un inicio y un final. Si aparece en color verde el programa será
cargado al conejo y éste ejecutará la ruta asignada.

Apagado
+++++++

Para apagar el sistema se recurre a poner los interruptores tanto del módulo principal como
los del conejo en apagado. Mientras esté apagado asegúrese de desconectar el adaptador del
módulo principal.

Resolución de Problemas
+++++++++++++++++++++++

* **P: El módulo central no enciende. - S**: Revise el cable de alimentación del módulo,
  primero revise que se encuentre conectado al toma corriente, seguidamente que se encuentre
  adecuadamente conectado al plug del módulo central.

* **P: El robot no enciende. - S**: Revise que las baterías insertadas en el robot se encuentren
  cargadas, seguidamente, revise que el interruptor de encendido se encuentre en la posición
  ON.

* **P: Las piezas no encajan. - S**: Localice el correspondiente par de conectores, uno
  hembra y otro macho, realice la conexión normalmente.

* **P: Ambos módulos se encuentran encendidos pero mi programan o se ejecuta.
  - S**: Revise que el programa haya sido correctamente leído al visualizar una luz verde en
  el módulo central. Si es así, desconecte el módulo central y apague el robot, seguidamente
  encienda el robot y en seguida conecte el módulo central.

Preguntas Frecuentes (FAQ Frecuentes (FAQ)
==========================================

* **¿A quién va dirigido BunnyCode?** BunnyCode está dirigido a niños entre 4 y 6 años
  para que aprendan jugando.

* **¿Para qué sirve BunnyCode?** El objetivo de BunnyCode es brindar un ambiente de
  aprendizaje divertido de aprendizaje de la programación a niños.

* **¿Quién está detrás de BunnyCode?** Detrás de BunnyCode hay un equipo de pedagógicos
  e ingenieros desarrollando herramientas y estrategias para la enseñanza de la
  programación.

* **¿Cómo puedo adquirir BunnyCode?** BunnyCode puede ser adquirido a través la campaña
  de crowdfunding en Indiegogo.

* **¿Dónde puedo comunicarme si necesito más información?** En el apartado 7 encontrarás
  los diferentes medios de contacto con BunnyCodeTeam.

Recomendaciones de Seguridad
============================

* BunnyCode puede ser usado por niños mayores a 4 años con la supervisión de un adulto
  relacionado con la operación segura, es importante que conozca los riesgos asociados a la
  operación del producto. La limpieza y el mantenimiento no deben ser hecho por niños a
  menos que sean mayores a 8 años y se encuentren bajo supervisión.

* BunnyCode puede ser usado por personas con capacidades sensoriales y físicas moderadas
  bajo la supervisión de un adulto responsable.

* BunnyCode no debe ser usado en lugares con fuentes de agua cercanas o ante la posibilidad
  de un derrame de algún líquido sobre él.

* Si el cable de la fuente de alimentación del BunnyCode está dañado, debe ser reemplazado
  por un centro autorizado.

* BunnyCode no debe ser sumergido en agua o en algún otro líquido.

* BunnyCode debe limpiarse con un paño seco y on abrasivo.

* Siempre desconecte BunyCode:

  * Inmediatamente después de terminar su uso.

  * Cuando esté siendo trasladado.

  * Antes de realizar la limpieza o el mantenimiento.

  *  Si no funciona correctamente.

* Ante cualquier problema o consulta, póngase en contacto con BunnyCode Team.

Contacto
========

A continuación se brinda información de contacto:

* **Address**: Bogotá - Colombia

* **Email**: bunnycodeproyect@gmail.com

* **Mobile**: +57 310 470 0900

* **Website**: https://www.bunny-code.com

Protege el Medio Ambiente
=========================

* BunnyCode ha sido diseñado para funcionar por mucho años, sin embargo, cuando decida
  ser reemplazado recuerda que puedes contribuir a proteger el medio ambiente.

* BunnyCode contiene materiales valiosos que puedes ser recuperados o reciclados.

* Deposítalo en un centro de recolección de residuos local.

